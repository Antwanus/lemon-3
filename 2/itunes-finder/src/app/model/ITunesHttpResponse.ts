import { Music } from "./Music";

export class ITunesHttpResponse {
  resultCount: number;
  results: Array<Music>;
}
