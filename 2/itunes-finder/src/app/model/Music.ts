export class Music {
    artistName?: string;
    artworkUrl100?: string;
    artworkUrl30?: string;
    artworkUrl60?: string;
    collectionArtistId?: number;
    collectionArtistViewUrl?: string;
    collectionCensoredName?:  string;
    collectionExplicitness?:  string;
    collectionHdPrice?: number;
    collectionId?: number;
    collectionName?: string;
    collectionPrice?: number;
    collectionViewUrl?: string;
    contentAdvisoryRating?: string;
    country?: string;
    currency?: string;
    discCount?: number;
    discNumber?: number;
    hasITunesExtras?: boolean;
    kind?: string;
    longDescription?: string;
    previewUrl?: string;
    primaryGenreName?: string;
    releaseDate?: string;
    shortDescription?: string;
    trackCensoredName?: string;
    trackCount?: number;
    trackExplicitness?: string;
    trackHdPrice?: number;
    trackHdRentalPrice?: number;
    trackId?: number;
    trackName?: string;
    trackNumber?: number;
    trackPrice?: number;
    trackRentalPrice?: number;
    trackTimeMillis?: number;
    trackViewUrl?: string;
    wrapperType?: string;


    static fromHttp(m:Music): Music {
      const newM = new Music();
      newM.artistName = m.artistName;
      newM.artworkUrl100 = m.artworkUrl100;
      newM.artworkUrl30 = m.artworkUrl30;
      newM.artworkUrl60 = m.artworkUrl60;
      newM.collectionArtistId = m.collectionArtistId;
      newM.collectionArtistViewUrl = m.collectionArtistViewUrl;
      newM.collectionCensoredName = m.collectionCensoredName;
      newM.collectionExplicitness = m.collectionExplicitness;
      newM.collectionHdPrice = m.collectionHdPrice;
      newM.collectionId = m.collectionId;
      newM.collectionName = m.collectionName;
      newM.collectionPrice = m.collectionPrice;
      newM.collectionViewUrl = m.collectionViewUrl
      newM.contentAdvisoryRating = m.contentAdvisoryRating;
      newM.country = m.country;
      newM.currency = m.currency;
      newM.discCount = m.discCount;
      newM.discNumber = m.discNumber;
      newM.hasITunesExtras = m.hasITunesExtras;
      newM.kind = m.kind;
      newM.longDescription = m.longDescription;
      newM.previewUrl = m.previewUrl;
      newM.primaryGenreName = m.primaryGenreName;
      newM.releaseDate = m.releaseDate;
      newM.shortDescription = m.shortDescription;
      newM.trackCensoredName = m.trackCensoredName;
      newM.trackCount = m.trackCount;
      newM.trackExplicitness = m.trackExplicitness;
      newM.trackHdPrice = m.trackHdPrice;
      newM.trackHdRentalPrice = m.trackHdRentalPrice;
      newM.trackId = m.trackId;
      newM.trackName = m.trackName;
      newM.trackNumber = m.trackNumber;
      newM.trackPrice = m.trackPrice;
      newM.trackRentalPrice = m.trackRentalPrice;
      newM.trackTimeMillis = m.trackTimeMillis;
      newM.trackViewUrl = m.trackViewUrl;
      newM.wrapperType = m.wrapperType;
      return newM;
    }

}
