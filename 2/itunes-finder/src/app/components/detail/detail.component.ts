import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Music } from 'src/app/model/Music';
import { ItunesApiService } from 'src/app/services/itunes-api.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
})
export class DetailComponent implements OnInit {
  record: Music;

  constructor(
    private dataService: ItunesApiService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      console.log('=> params are : ', params);
      const idParam: string = params['id'];
      if (idParam) {
        this.dataService.findMusicById(idParam).subscribe(
          (next) => {
            this.record = next;
          },
          (error) => {
            console.log('something went wrong :( ', error);
          }
        );
      } else {
        this.router.navigate(['']);
      }
    });
  }
  onClick() {
    this.router.navigate(['']);
  }
}
