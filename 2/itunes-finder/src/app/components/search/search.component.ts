import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Music } from 'src/app/model/Music';
import { ItunesApiService } from 'src/app/services/itunes-api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent implements OnInit {
  msgInvite: string = 'Please type in a filter';
  msgSearching: string = '';
  music: Array<Music>;
  selectedRecord: Music;

  constructor(
    private dataService: ItunesApiService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  onKeyUp(s: string) {
    if (s.length > 2) {
      this.msgSearching = 'Searching...';
      this.dataService.findMusic(s).subscribe(
        (next) => {
          console.log('dataservice => ', next);
          this.music = next;
          this.msgSearching = '';
        },
        (error) => {
          console.log('Something went wrong retrieving the data', error);
          this.msgSearching = 'Something went wrong :(';
        }
      );
    }
  }

  setSelectedRecord(id: string) {
    this.router.navigate(['detail'], {
      queryParams: { id: id },
    });
  }
  
  handleUrlChanged() {
    this.activatedRoute.url.subscribe((e)=> {
      console.log(e);

    })
  }
}
