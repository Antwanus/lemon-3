import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Music } from '../model/Music';
import { ITunesHttpResponse } from '../model/ITunesHttpResponse';

@Injectable({
  providedIn: 'root',
})
export class ItunesApiService {
  constructor(private http: HttpClient) {
    console.log('=> new DataService! environment = ', environment);
  }

  findMusic(s: string): Observable<Array<Music>> {
    const tmp = s.replace(' ', '+');
    return (
      this.http
        // https://itunes.apple.com/search?term=jack+johnson
        .get<ITunesHttpResponse>(environment.restUrl + '/search?term=' + tmp)
        .pipe(
          map((res) => {
            const musicArray = new Array<Music>();
            for (const result of res.results) {
              musicArray.push(Music.fromHttp(result));
            }
            return musicArray;
          })
        )
    );
  }
  findMusicById(id: string): Observable<Music> {
    return this.http
      .get<ITunesHttpResponse>(environment.restUrl + '/lookup?id=' + id)
      .pipe(
        map((res) => {
          const musicArray = new Array<Music>();
          for (const result of res.results) {
            musicArray.push(Music.fromHttp(result));
          }
          return musicArray[0];
        })
      );
  }
}
